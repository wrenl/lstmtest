import numpy as np
import sys

learning_rate = 0.02

def sigmoid(x, derv=False):
    if derv: return x * (1 - x)
    return 1 / (1 + np.exp(-x))

def tanh(x):
    return np.tanh(x)

def softmax(x):
    e_x = np.exp(x)
    return e_x / np.sum(e_x)


class LSTM:
    def __init__(self, d, h, y, seed = 0):
        np.random.seed(seed)

        self.Wf = np.random.normal(0, 1, (h, h + d))
        self.Wi = np.random.normal(0, 1, (h, h + d))
        self.Wg = np.random.normal(0, 1, (h, h + d))
        self.Wo = np.random.normal(0, 1, (h, h + d))
        self.Wv = np.random.normal(0, 1, (y, h))

        self.Bf = np.random.normal(0, 1, (h, 1))
        self.Bi = np.random.normal(0, 1, (h, 1))
        self.Bg = np.random.normal(0, 1, (h, 1))
        self.Bo = np.random.normal(0, 1, (h, 1))
        self.Bv = np.random.normal(0, 1, (y, 1))

        self.h_prev = np.zeros((h, 1))
        self.c_prev = np.zeros((h, 1))

    def forward(self, x):
        return self._forward(x)[0]

    def _forward(self, x):
        x = np.reshape(x, (x.shape[0],1))
        z = np.vstack((self.h_prev, x))

        f = sigmoid((self.Wf @ z) + self.Bf)
        i = sigmoid((self.Wi @ z) + self.Bi)
        g = tanh((self.Wg @ z) + self.Bg)
        o = sigmoid((self.Wo @ z) + self.Bo)

        c = f * self.c_prev + i * g
        h = o * tanh(c)

        y = softmax((self.Wv @ h) + self.Bv)

        c_prev = self.c_prev
        h_prev = self.h_prev

        self.c_prev = c
        self.h_prev = h

        return y, z, f, i, g, o, c_prev, h_prev, c, h


    def forward_backprop(self, m, x, y):
        yhat, z, f, i, g, o, c, h, c_next, h_next = self._forward(x)

        zT = z.T
        dy = yhat - y

        dC = dy * o * (1 - np.power(tanh(c), 2))

        #dh = dv @ self.Wv.T
        #dC = dh * o * (1 - c)

        do  = dy * tanh(c) * (o * (1 - o))
        dWo = do @ zT
        dBo = do

        dg  = dC * i * (1 - np.power(g, 2))
        dWg = dg * zT
        dBg = dg

        df  = dC * c * (f * (1 - f))
        dWf = df * zT
        dBf = df

        di  = dC * g * (i * (1 - i))
        dWi = di * zT
        dBi = di


        self.Wf -= learning_rate * (dWf / m)
        self.Wi -= learning_rate * (dWi / m)
        self.Wg -= learning_rate * (dWg / m)
        self.Wo -= learning_rate * (dWo / m)

        self.Bf -= learning_rate * (dBf / m)
        self.Bi -= learning_rate * (dBi / m)
        self.Bg -= learning_rate * (dBg / m)
        self.Bo -= learning_rate * (dBo / m)

        return yhat
####

X = np.array([
    [0, 1],
    [1, 0],
    [1, 1],
    [0, 0]
])

Y = np.array([
    [1],
    [1],
    [0],
    [0]
])

nn = LSTM(2, 4, 1, seed = 1)

m = len(X)

print("Wf\n", nn.Wf)
print("Wi\n", nn.Wi)
print("Wg\n", nn.Wg)
print("Wo\n", nn.Wo)

for i in range(1000):
    sys.stdout.write("\rIteration: {}".format(i))


    for j in range(m):
        y = nn.forward_backprop(m, X[j], Y[j])

print()
print("Wf\n", nn.Wf)
print("Wi\n", nn.Wi)
print("Wg\n", nn.Wg)
print("Wo\n", nn.Wo)

for j in range(m):
    y = nn.forward(X[j])
    print("{} => {}".format(X[j], y))


